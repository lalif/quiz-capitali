#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <string.h>

#define TOTCOU 196
typedef struct nation_tag {
  char *country;
  char *city;
} nation;

nation countries[TOTCOU] = {
{ "Afghanistan", "Kabul" },
{ "Albania", "Tirana" },
{ "Algeria", "Algeri" },
{ "Andorra", "Andorra la Vella" },
{ "Angola", "Luanda" },
{ "Antigua e Barbuda", "Saint John's" },
{ "Arabia Saudita", "Riyad" },
{ "Argentina", "Buenos Aires" },
{ "Armenia", "Erevan" },
{ "Australia", "Canberra" },
{ "Austria", "Vienna" },
{ "Azerbaigian", "Baku" },
{ "Bahamas", "Nassau" },
{ "Bahrein", "Manama" },
{ "Bangladesh", "Dacca" },
{ "Barbados", "Bridgetown" },
{ "Belgio", "Bruxelles" },
{ "Belize", "Belmopan" },
{ "Benin", "Porto-Novo" },
{ "Bhutan", "Thimphu" },
{ "Bielorussia", "Minsk" },
{ "Birmania", "Naypyidaw" },
{ "Bolivia", "La Paz" },
{ "Bosnia ed Erzegovina", "Sarajevo" },
{ "Botswana", "Gaborone" },
{ "Brasile", "Brasilia" },
{ "Brunei", "Bandar Seri Begawan" },
{ "Bulgaria", "Sofia" },
{ "Burkina Faso", "Ouagadougou" },
{ "Burundi", "Bujumbura" },
{ "Cambogia", "Phnom Penh" },
{ "Camerun", "Yaoundé" },
{ "Canada", "Ottawa" },
{ "Capo Verde", "Praia" },
{ "Rep. Ceca", "Praga" },
{ "Rep. Centrafricana", "Bangui" },
{ "Ciad", "N'Djamena" },
{ "Cile", "Santiago del Cile" },
{ "Cina", "Pechino" },
{ "Cipro", "Nicosia" },
{ "Colombia", "Bogotà" },
{ "Comore", "Moroni" },
{ "Rep. del Congo", "Brazzaville" },
{ "RD del Congo", "Kinshasa" },
{ "Corea del Nord", "Pyongyang" },
{ "Corea del Sud", "Seul" },
{ "Costa d'Avorio", "Yamoussoukro" },
{ "Costa Rica", "San José" },
{ "Croazia", "Zagabria" },
{ "Cuba", "L'Avana" },
{ "Danimarca", "Copenaghen" },
{ "Dominica", "Roseau" },
{ "Rep. Dominicana", "Santo Domingo" },
{ "Ecuador", "Quito" },
{ "Egitto", "Il Cairo" },
{ "El Salvador", "San Salvador" },
{ "Emirati Arabi Uniti", "Abu Dhabi" },
{ "Eritrea", "Asmara" },
{ "Estonia", "Tallinn" },
{ "Etiopia", "Addis Abeba" },
{ "Fiji", "Suva" },
{ "Filippine (Repubblica delle Filippine)", "Manila" },
{ "Finlandia", "Helsinki" },
{ "Francia", "Parigi" },
{ "Gabon", "Libreville" },
{ "Gambia", "Banjul" },
{ "Georgia", "Tbilisi" },
{ "Germania", "Berlino" },
{ "Ghana", "Accra" },
{ "Giamaica", "Kingston" },
{ "Giappone", "Tokyo" },
{ "Gibuti", "Gibuti" },
{ "Giordania", "Amman" },
{ "Grecia", "Atene" },
{ "Grenada", "Saint George's" },
{ "Guatemala", "Città del Guatemala" },
{ "Guinea", "Conakry" },
{ "Guinea-Bissau", "Bissau" },
{ "Guinea Equatoriale", "Malabo" },
{ "Guyana", "Georgetown" },
{ "Haiti", "Port-au-Prince" },
{ "Honduras", "Tegucigalpa" },
{ "India", "Nuova Delhi" },
{ "Indonesia (Repubblica di Indonesia)", "Giacarta" },
{ "Iran", "Teheran" },
{ "Iraq", "Baghdad" },
{ "Irlanda", "Dublino" },
{ "Islanda", "Reykjavík" },
{ "Isole Marshall", "Majuro" },
{ "Israele", "Gerusalemme" },
{ "Italia", "Roma" },
{ "Kazakistan", "Astana" },
{ "Kenya", "Nairobi" },
{ "Kirghizistan", "Bishkek" },
{ "Kiribati", "Bairiki" },
{ "Kuwait", "Madinat al-Kuwait" },
{ "Laos", "Vientiane" },
{ "Lesotho", "Maseru" },
{ "Lettonia", "Riga" },
{ "Libano", "Beirut" },
{ "Liberia", "Monrovia" },
{ "Libia", "Tripoli" },
{ "Liechtenstein", "Vaduz" },
{ "Lituania", "Vilnius" },
{ "Lussemburgo", "Lussemburgo" },
{ "Macedonia", "Skopje" },
{ "Madagascar", "Antananarivo" },
{ "Malawi", "Lilongwe" },
{ "Malaysia", "Kuala Lumpur" },
{ "Maldive", "Malé" },
{ "Mali", "Bamako" },
{ "Malta", "La Valletta" },
{ "Marocco", "Rabat" },
{ "Mauritania", "Nouakchott" },
{ "Mauritius", "Port Louis" },
{ "Messico", "Città del Messico" },
{ "Micronesia", "Palikir" },
{ "Moldavia", "Chisinau" },
{ "Monaco", "Monaco" }, 
{ "Mongolia", "Ulan Bator" },
{ "Montenegro", "Podgorica" },
{ "Mozambico", "Maputo" },
{ "Namibia", "Windhoek" },
{ "Nauru", "Yaren" },
{ "Nepal", "Kathmandu" },
{ "Nicaragua", "Managua" },
{ "Niger", "Niamey" },
{ "Nigeria", "Abuja" },
{ "Norvegia", "Oslo" },
{ "Nuova Zelanda", "Wellington" },
{ "Oman", "Mascate" },
{ "Regno dei Paesi Bassi", "Amsterdam" },
{ "Pakistan", "Islamabad" },
{ "Palau", "Ngerulmud" },
{ "Palestina", "Gerusalemme Est" },
{ "Panama", "Panama" },
{ "Papua Nuova Guinea", "Port Moresby" },
{ "Paraguay", "Asuncion" },
{ "Perù", "Lima" },
{ "Polonia", "Varsavia" },
{ "Portogallo", "Lisbona" },
{ "Qatar", "Doha" },
{ "Regno Unito", "Londra" },
{ "Romania", "Bucarest" },
{ "Ruanda", "Kigali" },
{ "Russia", "Mosca" },
{ "Saint Kitts e Nevis", "Basseterre" },
{ "Saint Vincent e Grenadine", "Kingstown" },
{ "Isole Salomone", "Honiara" },
{ "Samoa", "Apia" },
{ "San Marino", "San Marino" },
{ "Saint Lucia", "Castries" },
{ "São Tomé e Príncipe", "Sao Tomé" },
{ "Senegal", "Dakar" },
{ "Serbia", "Belgrado" },
{ "Seychelles", "Victoria" },
{ "Sierra Leone", "Freetown" },
{ "Singapore", "Singapore" }, 
{ "Siria", "Damasco" },
{ "Slovacchia", "Bratislava" },
{ "Slovenia", "Lubiana" },
{ "Somalia", "Mogadiscio" },
{ "Spagna", "Madrid" },
{ "Sri Lanka", "Sri Jayawardenapura Kotte" },
{ "Stati Uniti", "Washington" },
{ "Sudafrica", "Pretoria" },
{ "Sudan", "Khartum" },
{ "Sudan del Sud", "Juba" },
{ "Suriname", "Paramaribo" },
{ "Svezia", "Stoccolma" },
{ "Svizzera", "Berna" },
{ "Swaziland", "Mbabane" },
{ "Tagikistan", "Dušanbe" },
{ "Taiwan", "Taipei" },
{ "Tanzania", "Dodoma" },
{ "Thailandia", "Bangkok" },
{ "Timor Est", "Dili" },
{ "Togo", "Lomé" },
{ "Tonga", "Nukuʻalofa" },
{ "Trinidad e Tobago", "Port of Spain" },
{ "Tunisia", "Tunisi" },
{ "Turchia", "Ankara" },
{ "Turkmenistan", "Ashgabat" },
{ "Tuvalu", "Funafuti" },
{ "Ucraina", "Kiev" },
{ "Uganda", "Kampala" },
{ "Ungheria", "Budapest" },
{ "Uruguay", "Montevideo" },
{ "Uzbekistan", "Tashkent" },
{ "Vanuatu", "Port Vila" },
{ "Città del Vaticano", "Città del Vaticano" },
{ "Venezuela", "Caracas" },
{ "Vietnam", "Hanoi" },
{ "Yemen", "Sanaa" },
{ "Zambia", "Lusaka" },
{ "Zimbabwe", "Harare" }

  
};

int main() {
  int seed;
  char risposta[30];
  int i;
  int xn;
  int quiz;
  char  risposta2[2];
  seed = time(0);
  srand48(seed);

  /*  printf(" countries: %lu", sizeof(countries));
  
  for (xn=0; xn < TOTCOU; xn++) {
    printf("citta: %s country: %s\n", countries[xn].city, countries[xn].country); 
    }*/
  
  do {
    quiz = -1. +195. * lrand48() / RAND_MAX;
    printf ("%d\n" , quiz);
    printf("Qual è la capitale dello stato %s? \n" , countries[quiz].country);
    scanf("%s" , risposta);
    if (strcmp(risposta , countries[quiz].city) == 0) {
      printf("Esatto!\n");
    }
    else if (strcmp(risposta , "boh") == 0) {
      printf("Questa era difficile... la risposta esatta era %s.\n" , countries[quiz].city);
    }
    else {
      printf("Sbagliato! La risposta esatta era %s.\n" ,  countries[quiz].city);
    }

     printf("Vuoi giocare ancora (s/n) ?\n");
     scanf("%s" ,  risposta2);
     if (risposta2[0] == 'n') {
       printf("Arrivederci!\n");
       }
     } while (risposta2[0] == 's');

}
